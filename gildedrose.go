package gildedrose

import (
	"fmt"
	"strings"
)

type Item struct {
	Name            string
	SellIn, Quality int
}

func UpdateQuality(items []*Item) {
	for i := 0; i < len(items); i++ {
		UpdateItemQuality(items[i])
	}
}

func UpdateItemQuality(item *Item) {
	temp := item.Quality
	name := strings.ReplaceAll(item.Name, "Conjured ", "")
	switch name {
	case "Aged Brie":
		lowerSellIn(item)
		increaseQuality(item)
		if item.SellIn < 0 {
			increaseQuality(item)
		}
	case "Sulfuras, Hand of Ragnaros":
		fmt.Println("By fire, be purged !")
	case "Backstage passes to a TAFKAL80ETC concert":
		lowerSellIn(item)
		if item.SellIn > 0 {
			increaseQuality(item)
			if item.SellIn < 10 {
				increaseQuality(item)
				if item.SellIn < 5 {
					increaseQuality(item)
				}
			}
		} else {
			item.Quality = 0
		}
	default:
		lowerSellIn(item)
		lowerQuality(item)
	}

	// The 'Conjured' keyword will work for all items, like Conjured Aged Brie, which will go up in value twice as fast
	if strings.Contains(item.Name, "Conjured") {
		if !(item.Quality == 0) {
			item.Quality -= temp - item.Quality
		}
	}
}

func lowerSellIn(i *Item) {
	i.SellIn--
}
func lowerQuality(i *Item) {
	if !(i.Quality == 0) {
		i.Quality--
		if i.SellIn < 0 && !(i.Quality == 0) {
			i.Quality--
		}
	}
}
func increaseQuality(i *Item) {
	if !(i.Quality == 50) {
		i.Quality++
	}
}
