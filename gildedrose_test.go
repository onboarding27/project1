package gildedrose

import (
	"testing"
)

func TestUpdateQuality(t *testing.T) {
	t.Run("Once the sell by date has passed, Quality degrades twice as fast", func(t *testing.T) {
		//Given
		items := []*Item{
			{"NotPerished", 5, 10},
			{"GonnaPerish", 1, 10},
			{"Perished", -12, 10},
		}
		// When
		UpdateQuality(items)
		// Then
		assertEqual(t, items[0].Quality, 9)
		assertEqual(t, items[1].Quality, 9)
		assertEqual(t, items[2].Quality, 8)
	})
	t.Run("The Quality of an item is never negative", func(t *testing.T) {
		//Given
		items := []*Item{
			{"Low Quality Goblin Spear", 10, 0},
			{"Low Quality Goblin Spear", -5, 1},
		}
		// When
		UpdateQuality(items)
		// Then
		assertEqual(t, items[0].Quality, 0)
		assertEqual(t, items[1].Quality, 0)
	})
	t.Run("“Aged Brie” actually increases in Quality the older it gets", func(t *testing.T) {
		//Given
		items := []*Item{
			{"Aged Brie", 10, 10},  // Regular Brie
			{"Aged Brie", -12, 10}, // Should be rotten Brie
		}
		// When
		UpdateQuality(items)
		// Then
		assertEqual(t, items[0].Quality, 11)
		assertEqual(t, items[1].Quality, 12)
	})
	t.Run("The Quality of an item is never more than 50", func(t *testing.T) {
		//Given
		items := []*Item{
			{"Aged Brie", 10, 50},
			{"Backstage passes to a TAFKAL80ETC concert", 10, 49},
			{"Backstage passes to a TAFKAL80ETC concert", 5, 49},
		}
		// When
		UpdateQuality(items)
		// Then
		assertEqual(t, items[0].Quality, 50)
		assertEqual(t, items[1].Quality, 50)
		assertEqual(t, items[2].Quality, 50)
	})
	t.Run("“Sulfuras”, being a legendary item, never has to be sold or decreases in Quality", func(t *testing.T) {
		//Given
		items := []*Item{
			{"Sulfuras, Hand of Ragnaros", 10, 30},
		}
		// When
		UpdateQuality(items)
		// Then
		assertEqual(t, items[0].Quality, 30)
		assertEqual(t, items[0].SellIn, 10)
	})
	t.Run("“Backstage passes”, like aged brie, increases in Quality as its SellIn value approaches", func(t *testing.T) {
		//Given
		items := []*Item{
			{"Backstage passes to a TAFKAL80ETC concert", 20, 10},
			{"Backstage passes to a TAFKAL80ETC concert", 10, 10},
			{"Backstage passes to a TAFKAL80ETC concert", 5, 10},
			{"Backstage passes to a TAFKAL80ETC concert", 0, 10},
		}
		// When
		UpdateQuality(items)
		// Then
		assertEqual(t, items[0].Quality, 11)
		assertEqual(t, items[1].Quality, 12)
		assertEqual(t, items[2].Quality, 13)
		assertEqual(t, items[3].Quality, 0)
	})
	t.Run("“Conjured” items degrade in Quality twice as fast as normal items", func(t *testing.T) {
		//Given
		items := []*Item{
			{"Conjured bottle of wine", 10, 10},
			{"Conjured bottle of wine", -12, 10},
			{"Conjured Aged Brie", 10, 12},
			{"Conjured Backstage passes to a TAFKAL80ETC concert", 10, 10},
			{"Conjured Backstage passes to a TAFKAL80ETC concert", 5, 10},
			{"Conjured Backstage passes to a TAFKAL80ETC concert", 0, 10},
			{"Conjured Sulfuras, Hand of Ragnaros", 10, 30},
		}
		// When
		UpdateQuality(items)
		// Then
		assertEqual(t, items[0].Quality, 8)
		assertEqual(t, items[1].Quality, 6)
		assertEqual(t, items[2].Quality, 14)
		assertEqual(t, items[3].Quality, 14)
		assertEqual(t, items[4].Quality, 16)
		assertEqual(t, items[5].Quality, 0)
		assertEqual(t, items[6].Quality, 30)
	})
}

func assertEqual(t *testing.T, got, want int) {
	if got != want {
		t.Errorf("got %d want %d", got, want)
	}
}

// func Test_Foo(t *testing.T) {
// 	var items = []*gildedrose.Item{
// 		{"foo", 0, 0},
// 	}

// 	gildedrose.UpdateQuality(items)

// 	if items[0].Name != "fixme" {
// 		t.Errorf("Name: Expected %s but got %s ", "fixme", items[0].Name)
// 	}
// }
